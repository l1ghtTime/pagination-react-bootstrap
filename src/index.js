import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import { createStore, applyMiddleware } from 'redux';
import { logger } from "redux-logger";
import { Provider } from 'react-redux';
import rootReducer from './redux/reducers/rootReducer';
import reduxThunk from 'redux-thunk';

export const store = createStore(rootReducer, applyMiddleware(logger, reduxThunk));


const app = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(
  app,
  document.getElementById('root')
);

serviceWorker.unregister();
