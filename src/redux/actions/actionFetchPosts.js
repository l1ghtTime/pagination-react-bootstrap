import {FETCH_FLAG, CURRENT_POSTS, } from "./actionTypes"


// export function fetchPosts() {
//     return async dispatch => {
//         const response = await fetch('https://jsonplaceholder.typicode.com/posts');
//         const json = await response.json();
//         dispatch({ type: FETCH_POSTS, payload: json});
//     }
// }


export function addFetchFlag(value) {
    return {
        type: FETCH_FLAG,
        payload: value
    }
}


export function addCurrentPosts(value) {
    return {
        type: CURRENT_POSTS,
        payload: value,
    }
}
