import { combineReducers } from 'redux';
import reducerFetchPosts from './reducerFetchPosts';


export default combineReducers({
    reducerFetchPosts,
});