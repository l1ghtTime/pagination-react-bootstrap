import { FETCH_FLAG, CURRENT_POSTS, } from "../actions/actionTypes";


const initialState = {
    fetchFlag: false,
    currentPosts: null,
}

const reducerFetchPosts = (state = initialState, action) => {

    switch (action.type) {

        case FETCH_FLAG:
            return {
                ...state,
                fetchFlag: action.payload,
            }

        case CURRENT_POSTS:
            return {
                ...state,
                currentPosts: action.payload,
            }

        default:
            return state;
    }

};

export default reducerFetchPosts;
