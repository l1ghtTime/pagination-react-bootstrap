import React, { useState, useEffect } from 'react'
import Posts from './Posts/Posts';
import Pagination from './Pagination/Pagination';



const PageAllFirlms = () => {

    const [posts, setPosts] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(10);

    //Get current posts 

    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);
    

    useEffect(() => {
        async function fetchPosts() {
            let response = await fetch('https://jsonplaceholder.typicode.com/posts')
            response = await response.json()
            setPosts(response)
          }

        fetchPosts()
    }, []);

    //Change page

    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    return (
        <div className='container mt-5'>
            <h1 className='text-primary mb-3'>My Blog</h1>
            <Posts posts={currentPosts} />
            <Pagination postsPerPege={postsPerPage} totalPosts={posts.length} paginate={paginate} />
        </div>
    )
}

export default PageAllFirlms;
